﻿using Inf1008.Tp2.Core;

var run = true;
var solver = new Solver();

while (run)
{
    Console.Write("Entrez un nombre de reines (q pour quitter): ");
    var line = Console.ReadLine();

    switch (line)
    {
        case null:
        case "":
            Console.WriteLine("Entrée vide.");
            break;
        case "q":
            run = false;
            break;
        default:
            if (int.TryParse(line, out var n))
            {
                if (n < 1)
                {
                    Console.WriteLine("Le nombre de reines doit être supérieur à 0.");
                }
                else
                {
                    Console.Write("Voulez-vous afficher toutes les solutions? (y/N): ");
                    switch (Console.ReadLine())
                    {
                        case "y":
                        case "Y":
                            var solutions = solver.Solve(n, out var counter);
                            var solutionCount = 0;

                            foreach (var solution in solutions)
                            {
                                WriteSolution(solution);
                                solutionCount++;
                            }

                            Console.WriteLine("Nombre de k-prometteurs pour explorer toute l’espace de recherche: {0}", counter.Current);
                            Console.WriteLine("Nombre de solutions possibles: {0}", solutionCount);
                            break;
                        default:
                            var firstSolution = solver.Solve(n, out _).FirstOrDefault();
                            if (firstSolution != null)
                            {
                                WriteSolution(firstSolution);
                            }
                            else
                            {
                                Console.WriteLine("Aucune solution possible");
                            }
                            break;
                    }
                }
            }
            else
            {
                Console.WriteLine("Le nombre de reines doit être un entier.");
            }
            break;
    }
}

void WriteSolution(Solution solution)
{
    Console.Write(solution);
    Console.WriteLine("Nombre de k-prometteurs avant de trouver la solution ci-dessus: {0}", solution.Count);
}