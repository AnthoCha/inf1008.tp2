﻿using System.Collections;
using System.Collections.Generic;

namespace Inf1008.Tp2.Core
{
    public class Counter : IEnumerator<int>
    {
        private int _current = 0;

        public int Current
        {
            get
            {
                return _current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            _current++;
            return true;
        }

        public void Reset()
        {
            _current = 0;
        }
    }
}
