﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inf1008.Tp2.Core
{
    public class Solution
    {
        public Solution(IEnumerable<int> columns, int count)
        {
            Columns = columns;
            Count = count;
        }

        public IEnumerable<int> Columns { get; }
        public int Count { get; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            
            var n = Columns.Count();
            var line = new char[n];
            for (int i = 0; i < n; i++)
            {
                line[i] = '-';
            }

            foreach (var column in Columns)
            {
                line[column] = 'X';
                sb.AppendLine(string.Join("", line));
                line[column] = '-';
            }

            return sb.ToString();
        }
    }
}
