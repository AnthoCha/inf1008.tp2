﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Inf1008.Tp2.Core
{
    public class Solver
    {
        public IEnumerable<Solution> Solve(int n, out IEnumerator<int> counter)
        {
            if (n < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }

            counter = new Counter();
            return Solve(0, n, Enumerable.Empty<int>(), ImmutableHashSet.Create<int>(), ImmutableHashSet.Create<int>(), ImmutableHashSet.Create<int>(), counter);
        }

        private IEnumerable<Solution> Solve(int k, int n, IEnumerable<int> solution, IImmutableSet<int> columns, IImmutableSet<int> diag45, IImmutableSet<int> diag135, IEnumerator<int> counter)
        {
            if (k == n)
            {
                // Soustraire 1 pour compter seulement le nombre de k-prometteurs AVANT la solution
                yield return new Solution(solution, counter.Current - 1);
            }

            for (var j = 0; j < n; j++)
            {
                if (!columns.Contains(j) && !diag45.Contains(j - k) && !diag135.Contains(j + k))
                {
                    counter.MoveNext();
                    foreach (var s in Solve(k + 1, n, solution.Append(j), columns.Add(j), diag45.Add(j - k), diag135.Add(j + k), counter))
                    {
                        yield return s;
                    }
                }
            }
        }
    }
}
